package se.experis.NoticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.NoticeBoard.models.Comments;
import se.experis.NoticeBoard.models.Notice;
import se.experis.NoticeBoard.repositories.CommentRepository;
import se.experis.NoticeBoard.repositories.NoticeRepository;
import se.experis.NoticeBoard.repositories.UserRepository;
import se.experis.NoticeBoard.utils.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@RestController
public class NoticeController {

    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommentRepository commentRepository;

    //will load the specific site notice/id with correct information
    @GetMapping("/notice/{id}")
    public ResponseEntity<Notice> getEntryById(HttpServletRequest request, @PathVariable Integer id){

        Notice notice;
        HttpStatus resp;

        if(noticeRepository.existsById(id)) {
            System.out.println("Entry with id: " + id);
            notice= noticeRepository.getById(id);
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found");
            notice = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(notice, resp);
    }

    //this will create an new notice with a guid from the user. User must be signed in.
    @PostMapping("/notice")
    public ResponseEntity<Notice> addEntry(HttpServletRequest request, HttpSession session, @RequestBody Notice notice){

        HttpStatus resp;

        if (SessionKeeper.getInstance().CheckSession(session.getId())) {
            Cookie[] reqCookie = request.getCookies();
            for (Cookie cookie: reqCookie) {

                if (cookie.getName().equals("guid")) {
                    notice.guid = cookie.getValue();
                }
            }

            notice = noticeRepository.save(notice);

            System.out.println("New notice with id: " + notice.id);

             resp = HttpStatus.CREATED;
        } else {
           resp = HttpStatus.FORBIDDEN;
            System.out.println("Must sign in..");
        }


        return new ResponseEntity<>(notice, resp);
    }

    // Adds a comment to a notice. only signed in users may do this.
    @PostMapping("/add-comment")
    public ResponseEntity<Comments> addComment(HttpServletRequest request, HttpSession session, @RequestBody Comments comment){
        HttpStatus resp;
        if (SessionKeeper.getInstance().CheckSession(session.getId())) {
             resp = HttpStatus.CREATED;
            commentRepository.save(comment);
        } else {
            resp = HttpStatus.FORBIDDEN;
        }

       return new ResponseEntity<>(comment,resp);

    }

    //this will update the notice with a specific id. Also compare the users guid with the notice guid.
    @PatchMapping("/notice/{id}")
    public ResponseEntity<Notice> updateEntry(HttpServletRequest request, HttpSession session, @RequestBody Notice newEntry, @PathVariable Integer id) {

        Notice notice = null;
        HttpStatus resp;
        String guid = "";
        Cookie[] reqCookie = request.getCookies();
        for (Cookie cookie: reqCookie) {

            if (cookie.getName().equals("guid")) {
                guid = cookie.getValue();
            }
        }
        notice = noticeRepository.getById(id);

        if(noticeRepository.existsById(id) && SessionKeeper.getInstance().CheckSession(session.getId()) && guid.equals(notice.guid)) {
            Optional<Notice> noticeRepo = noticeRepository.findById(id);
            notice = noticeRepo.get();


            if(newEntry.text != null) {
                notice.text = newEntry.text;
            }

            noticeRepository.save(notice);

            System.out.println("Updated Entry with id: " + notice.id);
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found with id: " + id);
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(notice, resp);
    }

    //this will delete a notice with a specific id and check that only the user who created the notice can delete it.
    @DeleteMapping("/notice/{id}")
    public ResponseEntity<String> deleteEntry(HttpServletRequest request, HttpSession session, @PathVariable Integer id) {

        HttpStatus resp;
        String message = "";
        String guid = "";
        Cookie[] reqCookie = request.getCookies();
        for (Cookie cookie: reqCookie) {

            if (cookie.getName().equals("guid")) {
                guid = cookie.getValue();
            }
        }
        Notice notice = noticeRepository.getById(id);

        if(noticeRepository.existsById(id) && SessionKeeper.getInstance().CheckSession(session.getId()) && guid.equals(notice.guid)) {

            noticeRepository.deleteById(id);
            System.out.println("Deleted Entry with id: " + id);
            message = "SUCCESS";
            resp = HttpStatus.OK;
        } else {
            System.out.println("Fail");
            message = "FAIL";
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(message, resp);
    }
}
