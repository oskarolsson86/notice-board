package se.experis.NoticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.NoticeBoard.models.Notice;
import se.experis.NoticeBoard.models.UserName;
import se.experis.NoticeBoard.repositories.UserRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;


    @PostMapping("/user")
    public ResponseEntity<UserName> addUser(HttpServletRequest request, @RequestBody UserName username){

        username = userRepository.save(username);

        System.out.println("New notice with id: " + username.id);

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(username, resp);
    }



}
