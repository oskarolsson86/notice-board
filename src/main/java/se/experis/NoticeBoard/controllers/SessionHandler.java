package se.experis.NoticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se.experis.NoticeBoard.models.UserName;
import se.experis.NoticeBoard.repositories.UserRepository;
import se.experis.NoticeBoard.utils.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class SessionHandler {

    @Autowired
    private UserRepository userRepository;

    // Creates session and returns a guid and userName cookie to the client.
    @PostMapping("/saveSession")
    public String add(
            @RequestBody UserName reqBody,
            HttpServletResponse response,
            HttpSession session) {
            if(reqBody.password.equals("Craig")){
                System.out.println("password correct");
                SessionKeeper.getInstance().AddSession(session.getId());

                UserName user = userRepository.getByuserName(reqBody.userName);

                Cookie cookieName = new Cookie("userName", reqBody.userName);
                Cookie cookieGUID = new Cookie("guid", user.guid);
                cookieName.setMaxAge(600);
                cookieGUID.setMaxAge(600);
                response.addCookie(cookieName);
                response.addCookie(cookieGUID);

            }
            else {
                System.out.println("login failed");
            }
        return "index";
    }

    // logs out user and clear all cookies
    @PostMapping("/logout")
    public String logout(HttpSession session, HttpServletResponse response) {
        SessionKeeper.getInstance().RemoveSession(session.getId());

        System.out.println("logged out");
        Cookie userName = new Cookie("userName", "");
        userName.setMaxAge(0);
        response.addCookie(userName);
        Cookie guid = new Cookie("guid", "");
        guid.setMaxAge(0);
        response.addCookie(guid);
        Cookie sessionCookie = new Cookie("JSESSIONID", "");
        sessionCookie.setMaxAge(0);
        response.addCookie(sessionCookie);

        return "redirect:/";

    }

}
