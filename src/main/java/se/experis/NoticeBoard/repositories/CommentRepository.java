package se.experis.NoticeBoard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.NoticeBoard.models.Comments;


public interface CommentRepository extends JpaRepository<Comments, Integer> {
    Comments getById(int id);
}
