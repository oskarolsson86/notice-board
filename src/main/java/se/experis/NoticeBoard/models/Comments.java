package se.experis.NoticeBoard.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
@Entity
//@Table(name="Replies")
public class Comments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String replyText;

    @ManyToOne(fetch = FetchType.LAZY)
    public Notice notice;

    public Comments(){}
}