package se.experis.NoticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se.experis.NoticeBoard.models.Comments;
import se.experis.NoticeBoard.models.Notice;
import se.experis.NoticeBoard.repositories.CommentRepository;
import se.experis.NoticeBoard.repositories.NoticeRepository;

import java.util.List;


@Controller
public class ThymeleafController {

    @Autowired
    NoticeRepository noticeRepository;

    @Autowired
    CommentRepository commentRepository;


    //Creates entries-view. List entries gets everything in the entry class, sorts it by date and returns it
    @GetMapping("/")
    public String entriesView(Model model) {

        List<Notice> notices = noticeRepository.findAll();
        notices.sort(new Notice());
        model.addAttribute("notices", notices);

        return "index";
    }

    //Creates add-notice and refer to addentry.html
    @GetMapping("/add-entry")
    public String addEntry(Model model) {
        return "addentry";
    }

    //Creates edit/view/id and refer to edit.html
    @GetMapping("/edit-view/{id}")
    public String editEntry(@PathVariable int id, Model model) {
        Notice notice = noticeRepository.getById(id);
        model.addAttribute("notice", notice);

        model.addAttribute("comments", notice.replies);

        return "edit";
    }
}
