package se.experis.NoticeBoard.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Notice implements Comparator<Notice>{

    //automatically creates a new id
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String userName;

    @Column
    public String guid;

    //take the date for today and then sends it to String strDate
    @Column
    Date date = Calendar.getInstance().getTime();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public String strDate = dateFormat.format(date);

    //the text of the entry. changes text from varchar to text so that the user can put any lenght of text
    @Column(columnDefinition = "text" )
    public String text;

    @OneToMany(mappedBy="notice")
    public List<Comments> replies;

    //compare strdate (date) and will sort it so that the newest date is first
    @Override
    public int compare(Notice o1, Notice o2) {
        return o2.strDate.compareTo(o1.strDate);
    }
}
