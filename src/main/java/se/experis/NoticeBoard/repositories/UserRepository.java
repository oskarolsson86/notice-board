package se.experis.NoticeBoard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import se.experis.NoticeBoard.models.UserName;

public interface UserRepository extends JpaRepository<UserName, Integer> {
    UserName getById(int id);
    UserName getByuserName(String name);
}
